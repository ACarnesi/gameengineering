﻿using UnityEngine;
using System.Collections;

public class PickUp : MonoBehaviour {

	private PlayerHealth plrHlth;
	private Weapon wpn;
	private AudioSource pickUpAudioSource;
	private bool pickedUp;
	private float respawnTime;

	public AudioClip ammoPickUpClip;
	public AudioClip healthPickUpClip;
	public int pickUpValue = 25;

	void Awake () {
		pickUpAudioSource = this.gameObject.GetComponent<AudioSource>();
	}

	//Destroys pickUps when touched by player
	void OnTriggerEnter(Collider col) {
		//Gives the player health if this was a Health PickUp.
		if (col.tag == "Player" && this.tag == "Health") {
			respawnTime = 25.0f;
			PickUpHealth(col);
		} 

		//Gives the player ammo if this was an Ammo PickUp.
		else if (col.tag == "Player" && this.tag == "Ammo") {
			respawnTime = 25.0f;
			PickUpAmmo(col);
		}
	}

	void FixedUpdate () {
		//Rotates the GameObject if it is a Health PickUp.
		if(this.tag == "Health"){
			transform.Rotate(Vector3.up, Time.deltaTime * 10);
		}

		//"Respawns" the PickUp after a defined amount of time by re-enabling its Collider and MeshRenderer. 
		if(pickedUp){
			respawnTime -= Time.deltaTime;
			if(respawnTime <= 0){
				this.GetComponent<Collider>().enabled = true; 
				this.gameObject.GetComponent<MeshRenderer>().enabled = true;
				pickedUp = false;
			}
		}
	}

	//Plays the appropriate Health Sound and Disables the PickUp's Collider and MeshRenderer.
	void PickUpHealth(Collider col){
		pickUpAudioSource.clip = healthPickUpClip;
		pickUpAudioSource.Play();
		plrHlth = col.GetComponent<PlayerHealth>();
		plrHlth.AddHealth(pickUpValue);
		this.GetComponent<Collider>().enabled = false; 
		this.gameObject.GetComponent<MeshRenderer>().enabled = false; 
		pickedUp = true;
	}

	//Plays the appropriate Ammo Sound and Disables the PickUp's Collider and MeshRenderer.
	void PickUpAmmo(Collider col){
		pickUpAudioSource.clip = ammoPickUpClip;
		pickUpAudioSource.Play();
		wpn = col.GetComponentInChildren<Weapon>();
		wpn.AddAmmo(pickUpValue);
		this.GetComponent<Collider>().enabled = false; 
		this.gameObject.GetComponent<MeshRenderer>().enabled = false; 
		pickedUp = true;
	}
}


