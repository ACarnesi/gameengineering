﻿using UnityEngine;
using System.Collections;

public class LaserSight : MonoBehaviour {

	private LineRenderer laserSight;


	// Use this for initialization
	void Awake() {
		laserSight = this.GetComponent<LineRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		RaycastHit hit;
		laserSight.SetPosition(0, transform.position);

		if(Physics.Raycast(transform.position, transform.forward, out hit)){
			laserSight.SetPosition(1, hit.point);
//			laserSight.SetPosition(1, new Vector3(0,0,hit.distance));
		}
		else{
			laserSight.SetPosition(1, transform.forward * 20 + transform.position);
		}
	
	}
}
