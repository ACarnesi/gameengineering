﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BasicGUI : MonoBehaviour {

	public Text ammoToGUI;
	public Text youDiedText;
	public Text wepDescText;
	public Text generalInfo;

	private Weapon wpn;
	private PlayerHealth plrHlth;
	private GameObject player;
	private Slider healthSlider;
	private int currentAmmo; 
	private int maxAmmo;
	private float wepDescTime;


	void Start () {
		//This Locks the cursor on the screen to avoid distraction
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;

		//The YouDiedText is initially set to false as the player is not dead yet.
		youDiedText.enabled = false;

		//Finds the Player's GameObject to use it's components for the GUI
		//The Player health gives info about how much health the player has, while Weapon gives information concering the ammo remaining
		player = GameObject.FindWithTag("Player");
		plrHlth = player.GetComponent<PlayerHealth>();
		healthSlider = this.gameObject.GetComponentInChildren<Slider>();
		healthSlider.minValue = 0.0f;
		healthSlider.maxValue = plrHlth.startingHP;
		healthSlider.value = plrHlth.currentHP;
		wpn = player.GetComponentInChildren<Weapon>();
		maxAmmo = wpn.getMaxAmmo();
	}

	void Update () {
		//This updates the player's current health every frame and checks if the player is dead or not.
		healthSlider.value = plrHlth.currentHP;
		ammoToGUIMeth();

		//Display's the youDied text if the player has 0 HP
		if(plrHlth.IsDead()){
			youDiedText.enabled = true;
			plrHlth.gameObject.GetComponent<CharacterController>().enabled = false;
		}
		if(wepDescTime > 0){
			wepDescTime -= Time.deltaTime;
		}
		else if(wepDescTime <= 0){
			wepDescText.enabled = false;
		}
	}

	//Gets the remaining ammo the player has, and translates it to a string to use in the GUI Text
	void ammoToGUIMeth(){
		currentAmmo = wpn.getCurrentAmmo();
		ammoToGUI.text = ("Ammo Count: " + currentAmmo.ToString() + "/" + maxAmmo.ToString());
	}

	public void SetWepDesc(string s){
		wepDescText.enabled = true;
		wepDescTime = 5.0f;
		wepDescText.text = s;
	}
}