﻿using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour {

	public AudioClip meleeClip;
	public AudioClip gunshotClip;
	public int dmgAmt = 2; 
	public float speed = 2.0f;
	public float enemyAttackSpeed = 1.0f;
	public float fieldOfView = 110f;
	public Vector3 lastKnownPos;
	public Vector3[] patrolPath = new Vector3[1];
	public EnemyBehavior eB;
	public bool onAlert;
	public bool playerSighted;
	private Animator meleeAnimator;

	private GameObject player;
	private PlayerHealth plrHlth;
	private Transform wepTrans;
	private AudioSource enemyAudioSource;
	private ParticleSystem muzzleFlashParticle;
	private NavMeshAgent nav;
	private SphereCollider enemyCol;
	private Quaternion origRot;
	private float messageStutter;
	private float locationUpdateT;
	private float attackSpeed = 0;
	private float meleeRange = 2.5f;
	private float rangedRange = 12f;
	private float playerSearchTime;
	private int nextDest;

	public enum EnemyBehavior{
		PatrolGuard,
		StationGuard,
		SpawnGuard
	};


	void Awake () {
		//Finds the player's GameObject to use later for following it around
		player = GameObject.FindWithTag("Player");

		//Uses the player's PlayerHealth Script to call the deal damage function and remove HP.
		plrHlth = player.GetComponent<PlayerHealth>();

		//Initializes other Components. 
		enemyAudioSource = this.GetComponent<AudioSource>();
		enemyCol = this.GetComponent<SphereCollider>();
		enemyCol.radius = 20f;
		nav  = GetComponent<NavMeshAgent>();

		//Initializations of variables used for AI Behaviors. 
		origRot = transform.rotation;
		nextDest = 0;
		patrolPath[0] = transform.position;

		messageStutter = 0f;
		locationUpdateT = 1f;

		//Gets the appropriate graphical components depending on the enemy type
		if(this.tag == "RangedEnemy"){
			muzzleFlashParticle = this.gameObject.GetComponentInChildren<ParticleSystem>();
			GetComponent<Renderer>().material.SetColor("_Color", Color.red);
		}
		if(this.tag == "MeleeEnemy"){
			meleeAnimator = this.GetComponentInChildren<Animator>();
			meleeAnimator.enabled = false;
			GetComponent<Renderer>().material.SetColor("_Color", Color.yellow);
		}
	}

	void Update () {
		//If the Enemy does not see the player,set stopping Dist to 0.
		//This keeps the enemy from stopping pursuit while around a corner.
		if(messageStutter > 0){
			messageStutter -= Time.deltaTime;
		}

		if(!playerSighted){
			if(this.tag == "RangedEnemy"){
				nav.stoppingDistance = 0f;
			}
			else if(this.tag == "MeleeEnemy"){
				nav.stoppingDistance = 0f;
			}
		}

		//If the player is in sight, Look at the player, Move towards it, and stop at the appropriate distance.
		if(onAlert){

			if(playerSighted){
				LookAtPlayer();
				MoveTowardsPlayer();
				if(this.tag == "RangedEnemy"){
					nav.stoppingDistance = 6.0f;
				}
				else if(this.tag == "MeleeEnemy"){
					nav.stoppingDistance = 3.0f;
				}
			}
			
			//If the player is not sighted, and there is a lastKnownPos, go check out that position.
			else if(!playerSighted){

				if(locationUpdateT <= 0){
					locationUpdateT = 1f;
					lastKnownPos = player.GetComponent<IntelController>().globalLastPos;
				}

				else if(locationUpdateT > 0){
					locationUpdateT -= Time.deltaTime;
				}

				nav.SetDestination(lastKnownPos);
			}
		}

		//If not in pursuit, return to original set Behaviors.
		else if(!onAlert){ 
			if(eB == EnemyBehavior.PatrolGuard){
				OnPatrol();
			}
			else if(eB == EnemyBehavior.StationGuard){
				AtStation();
			}
			else if(eB == EnemyBehavior.SpawnGuard){
				ReturnToSpawn();
			}
		}
	}

	void OnTriggerStay(Collider col){
		//If the enemy is not disabled. 
		if(enabled == true){
			//Check for PlayerInSight.
			if(col.tag == "Player"){

				//By default the enemy does not see the player. 
				playerSighted = false;

				//Finds the player's position relative to the enemy. 
				//Then stores the angle between the direction the enemy is facing, and the player's relative position.
				Vector3 direction = player.transform.position - transform.position;
				float angle = Vector3.Angle(direction, transform.forward);

				//Checks if the player is within the field of view of the enemy.
				if(angle < fieldOfView * .5f){
					RaycastHit hit;

					//If the Player is inside the Enemie's Field of View, check if there are no obstacles blocking sight.
					if(Physics.Raycast (transform.position, direction.normalized, out hit, enemyCol.radius)){

						//If the tag of the hit object is Player, the player is sighted. Set last known position, and Attack.
						if(hit.transform.tag == "Player"){
							playerSighted = true;
							lastKnownPos = player.transform.position;
							EnemyAttack (hit.distance);
							if(messageStutter <= 0){
								messageStutter = .1f;
								player.GetComponent<IntelController>().OnAlert(this.gameObject);
							}
						}
					}
				}
			}
		}
	}

	//If the player leaves Enemy Range, he cannot be seen. 
	void OnTriggerExit(Collider col){
		if(col.tag == "Player"){
			playerSighted = false;
		}
	}

	//Set Enemy destination to the player's position.
	public void MoveTowardsPlayer(){
		nav.SetDestination(player.transform.position);
	}

	//Looks at the player.
	private void LookAtPlayer(){
		//Gets the displacement between the position of the enemy and its target
		Vector3 relativePos = player.transform.position - transform.position;
		relativePos.y = 0;
		
		//Gets the amount of rotation required for the enemy to face the target.
		Quaternion rotation = Quaternion.LookRotation (relativePos);
		
		
		//Rotates the enemy to face the target. 
		//This was used, opposed to lookAt to solve an issue I had when the enemy would turn vertically to look at the player. 
		//It would move the enemy away from the player to keep the enemy's orientation straight up. I suspect this was partly caused by NavMeshAgent.
		transform.rotation = rotation;
	}
	
	private void EnemyAttack(float dist){

		//This keeps track of how often the enemy attacks.
		attackSpeed -= Time.deltaTime;
		if(attackSpeed <= 0){
			//Plays the appropriate sounds and/or effects, and deals the set amount of damage to the Player if within attack Distance.
			if(this.tag == "MeleeEnemy"){
				meleeAnimator.enabled = false;
				if(dist <= meleeRange){
					enemyAudioSource.clip = meleeClip;
					enemyAudioSource.Play();
					plrHlth.TakeDamage (dmgAmt);
					meleeAnimator.enabled = true;
					attackSpeed = enemyAttackSpeed; 
				}
			}
			
			//Plays the appropriate sounds and/or effects and deals the set amount of damage to the Player if within attack Distance.
			else if(this.tag == "RangedEnemy"){
				if(dist <= rangedRange){
					muzzleFlashParticle.Emit (100);
					enemyAudioSource.clip = gunshotClip;
					enemyAudioSource.Play();
					attackSpeed = enemyAttackSpeed; 
					plrHlth.TakeDamage (dmgAmt);
				}
			}
		}
	}

	//Keeps the enemy guarding one point.
	private void AtStation(){
		nav.SetDestination(patrolPath[0]);

		//If the unit has returned to its original position, it will turn to its original set Rotation.
		if(transform.position.x == patrolPath[0].x && transform.position.z == patrolPath[0].z){
			transform.rotation = Quaternion.Slerp (transform.rotation, origRot, 2f);
		}
	}

	//Keeps an enemy on a pre-defined patrol path.
	private void OnPatrol(){
		//Go to next Destination
		nav.SetDestination(patrolPath[nextDest]);

		//If reached next Destination, start moving to the next destination. 
		if(transform.position.x == patrolPath[nextDest].x && transform.position.z == patrolPath[nextDest].z){
			nextDest++;
			//Keeps from going past array length. 
			if(nextDest != patrolPath.Length){
				nav.SetDestination(patrolPath[nextDest]);
			}

			//Resets to starting position of patrol.
			else if(nextDest >= patrolPath.Length){
				nextDest = 0;
				nav.SetDestination(patrolPath[nextDest]);
			}
		}
	}

	//Returns spawned enemies to spawn when doing nothing else. 
	public void ReturnToSpawn(){
		nav.SetDestination(patrolPath[0]);
	}
}
