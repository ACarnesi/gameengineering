﻿using UnityEngine;
using System.Collections;

public class TextPrompt : MonoBehaviour {

	public string prompt;

	private BasicGUI gUI;

	void Awake(){
		gUI = GameObject.FindGameObjectWithTag("Canvas").GetComponent<BasicGUI>();
	}

	void OnTriggerStay(Collider col){
		if(col.tag == "Player"){
			gUI.generalInfo.enabled = true;
			gUI.generalInfo.text = prompt;
		}
	}
	
	void OnTriggerExit(Collider col){
		if(col.tag == "Player"){
			gUI.generalInfo.enabled = false;
		}
	}
}
