﻿using UnityEngine;
using System.Collections;

public class IntelController : MonoBehaviour {

	public Vector3 globalLastPos;

	private bool onAlert;
	private GameObject alertedEnemy;
	private float alertTime;
	private int enemyCount;
	private GameObject[] enemyArray;



	// Use this for initialization
	void Awake () {
		alertTime = 0;
		onAlert = false;

		enemyCount = 0;
		GameObject[] rangedArray = GameObject.FindGameObjectsWithTag("RangedEnemy");
		enemyCount += rangedArray.Length;
		GameObject[] meleeArray = GameObject.FindGameObjectsWithTag("MeleeEnemy");
		enemyCount+= meleeArray.Length;
		GameObject[] spawnerArray = GameObject.FindGameObjectsWithTag("Spawner");
		enemyCount+= spawnerArray.Length;
		GameObject[] turretArray = GameObject.FindGameObjectsWithTag("Turret");
		enemyCount+= turretArray.Length;

		enemyArray = new GameObject[enemyCount];

		int arrayPlace = 0;
		for(int i = 0; i < rangedArray.Length; i++){
			enemyArray[arrayPlace] = rangedArray[i];
			arrayPlace++;
		}

		for(int i = 0; i < meleeArray.Length; i++){
			enemyArray[arrayPlace] = meleeArray[i];
			arrayPlace++;
		}

		for(int i = 0; i < spawnerArray.Length; i++){
			enemyArray[arrayPlace] = spawnerArray[i];
			arrayPlace++;
		}

		for(int i = 0; i < turretArray.Length; i++){
			enemyArray[arrayPlace] = turretArray[i];
			arrayPlace++;
		}
	}

	void Update(){
		if(onAlert){
			alertTime -= Time.deltaTime;
			Debug.Log (alertTime.ToString());
			if(alertTime <= 0){
				onAlert = false;
				Disengage();
			}
		}
	}
	
	// Update is called once per frame
	public void OnAlert (GameObject sender) {
		Debug.Log ("ALERT!");
		onAlert = true;
		alertTime = 7.5f;
		if(sender.tag != "Turret"){
			globalLastPos = sender.GetComponent<EnemyAI>().lastKnownPos;
		}
		else if(sender.tag == "Turret"){
			globalLastPos = sender.GetComponent<TurretMotor>().lastKnownPos;
		}
		

		for(int i = 0; i < enemyArray.Length; i++){
			if(enemyArray[i] != null){
				float distance = Vector3.Distance(sender.transform.position, enemyArray[i].transform.position);
				if(distance < 20){
					if(enemyArray[i].tag == "MeleeEnemy" || enemyArray[i].tag == "RangedEnemy"){
						enemyArray[i].GetComponent<EnemyAI>().onAlert = true;
						enemyArray[i].GetComponent<EnemyAI>().lastKnownPos = globalLastPos;
					}
					else if(enemyArray[i].tag == "Spawner"){
						enemyArray[i].GetComponent<EnemySpawner>().activated = true;
						enemyArray[i].GetComponent<EnemySpawner>().lastKnownPos = globalLastPos;
					}
					else if(enemyArray[i].tag == "Turret"){
						enemyArray[i].GetComponent<TurretMotor>().onAlert = true;
						enemyArray[i].GetComponent<TurretMotor>().lastKnownPos = globalLastPos;
					}
				}
			}
		}
	}

	void Disengage(){
		foreach(GameObject enemy in enemyArray){
			if(enemy !=  null){
				if(enemy.tag == "RangedEnemy" || enemy.tag == "MeleeEnemy"){
					enemy.GetComponent<EnemyAI>().onAlert = false;
				}
				else if(enemy.tag == "Spawner"){
					enemy.GetComponent<EnemySpawner>().activated = false;
				}
			}
		}
	}
}
