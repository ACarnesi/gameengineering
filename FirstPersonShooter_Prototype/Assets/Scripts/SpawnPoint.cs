﻿using UnityEngine;
using System.Collections;

public class SpawnPoint : MonoBehaviour 
{
	Transform player;

	// Use this for initialization
	void Start () 
	{
		player = GameObject.FindGameObjectWithTag("Player").transform;
		transform.position = player.position;
	}

}
