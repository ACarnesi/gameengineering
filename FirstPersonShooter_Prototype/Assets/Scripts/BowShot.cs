﻿using UnityEngine;
using System.Collections;

public class BowShot : MonoBehaviour {

	private Rigidbody arrowRig;
	private PlayerHealth plrHlth;
	private Weapon wep;
	private float bowC;

	void Awake () {
		//Gets the charge the bow was at when this bowShot was fired.
		bowC = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<Weapon>().getBowCharge();

		//Initializes arrowRig to this gameObjects RigidBody.
		arrowRig = this.gameObject.GetComponent<Rigidbody>();

		//Adds a force to the bowShot multiplied by the charge of the Bow when it was fired. 
		arrowRig.AddRelativeForce(Vector3.forward * (bowC * 2000));
	}

	void OnCollisionEnter (Collision col) {
		//If the bowShot collides with an Enemy, deal damage based off of the charge the bow was at on fireing.
		if(col.gameObject.tag == "MeleeEnemy" || col.gameObject.tag == "RangedEnemy" || col.gameObject.tag == "Turret"){
			plrHlth = col.gameObject.GetComponent<PlayerHealth>();
			float dmgAmtF = bowC * 10f;
			int dmgAmt = (int)dmgAmtF;
			plrHlth.TakeDamage (dmgAmt);
		}
		//Destroy if this bowShot hits anything. 
		Destroy (this.gameObject);
	}
}
