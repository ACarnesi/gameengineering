﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

	//Defining needed Variables
	public GameObject enemyPref;
	public int maxSpawnNum;
	public bool activated;
	public Renderer spawnerTopRend;
	public Vector3 lastKnownPos;

	private IntelController iC;
	private int enemyCycle;
	private int spawnedNum = 0;
	private Vector3 spawnPoint;
	private float spawnTimer;
	private GameObject[] spawnedEnemies;

	void Start () {
		iC = GameObject.FindGameObjectWithTag("Player").GetComponent<IntelController>();
		//Adjusting for transform position of Spawner so that Enemies don't spawn inside the floor
		spawnPoint = new Vector3(this.transform.position.x, this.transform.position.y + 1f, this.transform.position.z);

		//Sets the length of the spawnedEnemies array. This is used to keep track of which enemies are destroyed, or alive later.
		spawnedEnemies = new GameObject[maxSpawnNum];

		//Initializes variables.
		spawnedNum = 0;
		enemyCycle = 0;

		//Sets the appropriate color for the spawner, depending on its Spawn Type.
		if(enemyPref.tag == "RangedEnemy"){
			GetComponent<Renderer>().material.SetColor("_Color", Color.red);
			spawnerTopRend.material.SetColor("_Color", Color.red);
		}
		else if(enemyPref.tag == "MeleeEnemy"){
			GetComponent<Renderer>().material.SetColor("_Color", Color.yellow);
			spawnerTopRend.material.SetColor("_Color", Color.yellow);
		}
	}

	void Update () {
		//If the Spawner is activated, spawn a unit instantly, and one every second there after. 
		//The spawner is activated inside of EnemyAI.
		if(activated){
			//Countdown to next spawn.
			spawnTimer -= Time.deltaTime;

			//If the max number of spawns is not reached, continue spawning. 
			if(spawnTimer <= 0 && spawnedNum < maxSpawnNum){
				Spawn();
			}

			foreach(GameObject enemy in spawnedEnemies){
				if(enemy != null){
					enemy.GetComponent<EnemyAI>().onAlert = true;
				}
				else if (enemy == null && spawnedNum >= maxSpawnNum){
					spawnedNum--;
				}
			}
		}


		//If the spawner becomes deactivated, queue the despawn.
		else if(!activated){
			DeSpawn ();
		}
	}

	//This Instantiates a predefined GameObject prefab and resets the spawn time so that they do not all spawn at once
	void Spawn(){
		for(int i = 0; i < maxSpawnNum; i++){
			if(spawnedEnemies[i] != null){
				spawnedEnemies[i].GetComponent<EnemyAI>().onAlert = true;
			}
			//A unit will only be spawned if the value in the array is not null AKA an Enemy has not already been spawned in that position.
			else if(spawnedEnemies[i] == null){
				spawnedEnemies[i] = (Instantiate(enemyPref, spawnPoint, new Quaternion(0,0,0,0)) as GameObject);
				spawnedEnemies[i].GetComponent<EnemyAI>().onAlert = true;
				spawnedEnemies[i].GetComponent<EnemyAI>().lastKnownPos = iC.globalLastPos;
				spawnTimer = 1.0f;
				spawnedNum++;
				break;
			}
		}
	}

	//Destroys all remaining spawned enemies. 
	void DeSpawn(){
		if(spawnedNum != 0){
			for(int i = 0; i < maxSpawnNum; i++){
				if(spawnedEnemies[i] != null){
					spawnedEnemies[i].GetComponent<EnemyAI>().onAlert = false;
					float distance = Vector3.Distance(this.transform.position, spawnedEnemies[i].transform.position);
					if(distance <= 2f){
						Destroy(spawnedEnemies[i]);
						spawnedNum--;
					}
				}
			}
		}
	}
}
