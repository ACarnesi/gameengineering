﻿using UnityEngine;
using System.Collections;

public class TurretMotor : MonoBehaviour {
	
	public AudioClip turretGunAudio;
	public Vector3 lastKnownPos;
	public float speed;
	public bool onAlert; 

	private float increasedFireRate;
	private float fireRate;
	private int dmgAmt;
	private PlayerHealth plrHlth;
	private AudioSource turretGunSource;
	private ParticleSystem muzzleFlashParticle;

	// Use this for initialization
	void Awake () {
		//Sets the ParticleSystem and Audio source to the respective components on the object.
		muzzleFlashParticle = this.gameObject.GetComponentInChildren<ParticleSystem>();
		turretGunSource = this.gameObject.GetComponent<AudioSource>(); 
		turretGunSource.clip = turretGunAudio;
		//Sets the color of the turret to red.
		GetComponent<Renderer>().material.SetColor("_Color", Color.red);

		//Sets fire variables to predefined amounts.
		fireRate = 1.0f; 
		increasedFireRate = fireRate;
		dmgAmt = 1; 
	}

	void Update(){
		if(onAlert){
			this.gameObject.GetComponent<SphereCollider>().radius = 30f;
		}

		else if(!onAlert){
			this.gameObject.GetComponent<SphereCollider>().radius = 20f;
		}
	}

	void OnTriggerEnter(Collider col){
		if(col.tag == "Player"){
			lastKnownPos = col.transform.position;
			col.gameObject.GetComponent<IntelController>().OnAlert(this.gameObject);
		}
	}

	void OnTriggerStay(Collider col){
		if(enabled == true){
			//If the player enters the collider shoot at it.
			if(col.tag == "Player"){
				//sets how fast the turret fires at its target.
				float step = speed * Time.deltaTime;
				
				//Gets the displacement between the position of the turret and its target
				Vector3 relativePos = col.transform.position - transform.position;
				
				//Gets the amount of rotation required for the turret to face the target.
				Quaternion rotation = Quaternion.LookRotation (relativePos);
				
				//Rotates the turret to face the target. I used this method opposed to LookAt so that it would be a smoother transition into the turret facing the player, and so that if the player was moving fast enough it could avoid some of the shots of the turret.
				transform.rotation = Quaternion.Slerp(transform.rotation, rotation, step);
				RaycastHit hit; 
				
				//Once fireRate reaches 0, it will fire at its target.
				fireRate -= Time.deltaTime;
				
				//It will play its gunshot sound and particle muzzle flash if the raycast hits the player. 
				if (fireRate <= 0 && Physics.Raycast(transform.position, transform.forward, out hit)){
					if(hit.transform.tag == "Player"){
						turretGunSource.Play();
						muzzleFlashParticle.Emit(100);
						plrHlth = col.GetComponent<PlayerHealth>();
						
						//Decreases the time required to fire by a factor of .75 until it takes less than a quarter of a second to fire. 
						if(increasedFireRate > .25f){
							increasedFireRate *= .75f;
							fireRate = increasedFireRate;
						}
						else{
							fireRate = increasedFireRate;
						}
						
						//Deal the predefined amount of damage to the Player.
						plrHlth.TakeDamage(dmgAmt);
						
					}
				}
			}
		}
	}

	//Resets the fire rate back to its original value when the player leaves the trigger zone. 
	void OnTriggerExit(Collider col){
		if(col.tag == "Player"){
			fireRate = 1.0f; 
			increasedFireRate = fireRate; 
		}
	}
}
