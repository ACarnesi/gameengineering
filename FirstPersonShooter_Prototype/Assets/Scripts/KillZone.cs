﻿using UnityEngine;
using System.Collections;

public class KillZone : MonoBehaviour 
{
	Transform SP;

	void Start()
	{
		SP = GameObject.FindGameObjectWithTag("SpawnPoint").transform;
	}

	void OnTriggerEnter (Collider col)
	{
		if(col.gameObject.tag == "Player")
		{
			col.transform.position = SP.position;
			//Application.LoadLevel(Application.loadedLevel);
		}
	}
}
