﻿using UnityEngine;
using System.Collections;

public class CheckPoint : MonoBehaviour 
{
	Transform SP;
	public Vector3 SPOffset;

	void Start () 
	{
		SP = GameObject.FindGameObjectWithTag("SpawnPoint").transform;
	}
	
	void OnTriggerEnter (Collider col) 
	{
		if(col.gameObject.tag == "Player")
		{
			SP.position = new Vector3(transform.position.x + SPOffset.x, transform.position.y + SPOffset.y, transform.position.z + SPOffset.z) ;
			Destroy(this.gameObject);
		}
	}
}
