﻿using UnityEngine;
using System.Collections;

public class EndGoal : MonoBehaviour {

	// Restarts the Level upon trigger
	void OnTriggerEnter(Collider col){
		if(col.tag == "Player"){
			Application.LoadLevel(Application.loadedLevel);
		}
	}
}
