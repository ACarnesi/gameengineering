﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Weapon : MonoBehaviour {

	public int dmgAmt;
	public AudioClip glockAudioClip;
	public AudioClip nullifierAudioClip;
	public AudioClip bowAudioClip;
	public AudioClip dryFireAudioClip;
	public GameObject bulletHole;
	public GameObject nullifierShot;
	public GameObject bowShot;
	public Renderer gunRend;

	private PlayerHealth plrHlth;
	private int ammoCount;
	private int maxAmmo = 250;
	private float bowCharge;
	private float disableTime;
	private AudioSource gunAudioSource;
	private ParticleSystem muzzleFlashParticle;
	private WeaponType selecWep;
	private BasicGUI gUI;

	//Used to check which weapon is selected.
	enum WeaponType{
		Glock,
		Nullifier,
		Bow
	};


	void Awake () {
		//Setting the componenets for later use. 
		muzzleFlashParticle = this.gameObject.GetComponent<ParticleSystem>();
		gunAudioSource = this.gameObject.GetComponent<AudioSource>(); 
		gunRend = GameObject.FindGameObjectWithTag("Gun").GetComponent<Renderer>();
		//The player starts with the Glock weapon selected.
		gunRend.material.SetColor("_Color", Color.black);
		selecWep = WeaponType.Glock;

		//The player starts with 25 ammo.
		ammoCount = 25;

		gUI = GameObject.FindGameObjectWithTag("Canvas").GetComponent<BasicGUI>();
		gUI.SetWepDesc("Selected Weapon: Glock \n Description: Fires single bullets to damage targets.");
	}

	void Update () {
		//Checks which weapon is selected, and call the appropriate fire method. 
		if (selecWep == WeaponType.Glock) {
			FireGlock();
		}
		else if(selecWep == WeaponType.Nullifier){
			FireNullifier();
		}
		else if(selecWep == WeaponType.Bow){
			FireBow();
		}

		//Checks if the player has switched weapons.
		SwitchWeapon();
	}

	//Adds ammo to the player's weapon up to a predefined maximum. This is called in the pickUp script. 
	public void AddAmmo(int ammoAmt){
		ammoCount += ammoAmt;
		if(ammoCount > maxAmmo){
			ammoCount = maxAmmo;
		}
	}

	//If the player presses one of the numbers below, it will select the respective weapon for that number.
	public void SwitchWeapon(){
		if(Input.GetKeyDown (KeyCode.Alpha1)){
			selecWep = WeaponType.Glock;
			gunRend.material.SetColor("_Color", Color.black);
			gUI.SetWepDesc("Selected Weapon: Glock \n Description: Fires single bullets to damage targets.");
		}
		else if(Input.GetKeyDown (KeyCode.Alpha2)){
			selecWep = WeaponType.Nullifier;
			gunRend.material.SetColor("_Color", Color.magenta);
			gUI.SetWepDesc("Selected Weapon: Nullifier \n Description: Disables targets.");
		}
		else if(Input.GetKeyDown (KeyCode.Alpha3)){
			bowCharge = 0;
			selecWep = WeaponType.Bow;
			gunRend.material.SetColor("_Color", Color.green);
			gUI.SetWepDesc("Selected Weapon: Bow \n Description: Hold fire to charge shots, making them fly faster and cause more damage.");
		}
	}

	//Fires Glock
	public void FireGlock(){
		//Shoot when left click is pressed
		if (Input.GetMouseButtonDown (0)) {
			//If ammo is less than or equal to 0 do not shoot and play dryFire sound.
			if (ammoCount <= 0) {
				gunAudioSource.clip = dryFireAudioClip;
				gunAudioSource.Play ();
			}
			//If the player does have ammo, subtract 1 from ammoCount and fire.
			else {
				ammoCount--;
				if(ammoCount < 0){
					ammoCount = 0;
				}
				//Play gunshot sound.
				gunAudioSource.clip = glockAudioClip;
				gunAudioSource.Play ();
				
				//Cause particle emition for muzzleFlash effect.
				muzzleFlashParticle.Emit(100);

				RaycastHit hit;
				//If the raycast hits something, set plrHlth to that objects PlayerHealth script.
				if (Physics.Raycast (transform.position, transform.forward, out hit)) {
					plrHlth = hit.transform.gameObject.GetComponent<PlayerHealth> ();
					//Instantiate a bulletHole object at the location it hit, and child it to the object it hit. 
					GameObject myBulletHole = Instantiate (bulletHole, hit.point, Quaternion.LookRotation (hit.normal)) as GameObject;
					myBulletHole.transform.parent = hit.transform;
					//Check the tag of the collider that the raycast came in contact with 
					if (hit.transform.tag == "Turret" || hit.transform.tag == "RangedEnemy" || hit.transform.tag == "MeleeEnemy") {
						//Damage the object by calling the TakeDamage method from the PlayerHealth script.
						plrHlth.TakeDamage (dmgAmt);
					}
				}
			}
		}
	}

	//Fires Nullifier Weapon
	public void FireNullifier(){
		//Shoot when left click is pressed
		if (Input.GetMouseButtonDown (0)) {
			//If ammo is less than or equal to 0 do not shoot and play dryFire sound.
			if (ammoCount <= 0) {
				gunAudioSource.clip = dryFireAudioClip;
				gunAudioSource.Play ();
			}
			//If the player does have ammo, subtract 5 from ammoCount and fire.
			else {
				ammoCount -= 5;
				if(ammoCount < 0){
					ammoCount = 0;
				}
				
				//Play gunshot sound.
				gunAudioSource.clip = nullifierAudioClip;
				gunAudioSource.Play ();
				//Cause particle emition for muzzleFlash effect.
				muzzleFlashParticle.Emit (100);
				RaycastHit hit;
				
				if (Physics.Raycast (transform.position, transform.forward, out hit)) {
					//Instantiates a NullifierShot and scales it the distance the raycast hit.
					GameObject myNullifierShot = Instantiate (nullifierShot, this.transform.position, this.transform.rotation) as GameObject;
					myNullifierShot.transform.localScale = new Vector3(myNullifierShot.transform.localScale.x, myNullifierShot.transform.localScale.y, hit.distance);

					//Check the tag of the collider that the raycast came in contact with 
					if (hit.transform.tag == "Turret" || hit.transform.tag == "RangedEnemy" || hit.transform.tag == "MeleeEnemy") {
						//Sets the value of plrHlth to the PlayerHealth Component of the GameObject that was hit.
						plrHlth = hit.transform.gameObject.GetComponent<PlayerHealth> ();
						//Disables the GameObject.
						plrHlth.SetDisable();
					}
					//If the GameObject was a Spawner, set its color to Grey.
					else if(hit.transform.tag == "SpawnerTop"){
						hit.transform.GetComponent<Renderer>().material.SetColor("_Color", Color.grey);
						//Sets the value of plrHlth to the PlayerHealth Component of the GameObject that was hit.
						plrHlth = hit.transform.GetComponentInParent<PlayerHealth>();
						//Disables the GameObject.
						plrHlth.SetDisable ();
					}
				}
				//If no GameObject was hit by the raycast, instantiate a nullifierShot with a scale of 50.
				else{
					GameObject myNullifierShot = Instantiate (nullifierShot, this.transform.position, this.transform.rotation) as GameObject;
					myNullifierShot.transform.localScale = new Vector3(myNullifierShot.transform.localScale.x, myNullifierShot.transform.localScale.y, 100);
				}
			}
		}
	}

	//Fires Bow Weapon
	public void FireBow(){
		//Begin charging while the left mouse button is held.
		if(Input.GetMouseButton (0)){
			//Charges to max once 2 seconds have passed.
			if(bowCharge <= 2f){
				bowCharge += Time.deltaTime;
			}
		}

		//Fire the bhowShot when the left mouse button is released.
		if (Input.GetMouseButtonUp (0)) {
			//If ammo is less than or equal to 0 do not shoot and play dryFire sound.
			if (ammoCount <= 0) {
				gunAudioSource.clip = dryFireAudioClip;
				gunAudioSource.Play ();
			}
			//If the player does have ammo, subtract 1 from ammoCount and fire.
			else {
				ammoCount--;
				if(ammoCount < 0){
					ammoCount = 0;
				}

				//Instantiates a bowShot. This object is given force at Initialization.
				GameObject myBowShot = Instantiate (bowShot, this.transform.position, this.transform.rotation) as GameObject;
				//Play gunshot sound.
				gunAudioSource.clip = bowAudioClip;
				gunAudioSource.Play ();
			}
			//Reset the bowCharge to 0.
			bowCharge = 0;
		}
	}

	//Gets the current ammo of the weapon. 
	public int getCurrentAmmo(){
		return ammoCount; 
	}

	//Gets the max ammo of the weapon.
	public int getMaxAmmo(){
		return maxAmmo;
	}

	//Gets the current Charge of the bow.
	public float getBowCharge(){
		return bowCharge;
	}
}
