﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {

	public int startingHP;
	public int currentHP = 10;

	private RawImage dmgImg;
	private float dmgImgTime;
	private float deathTime;
	private bool isDead = false;
	private float disableTime;
	private GameObject player;

	void Awake () {
		//sets the hitPoints to the desired number
		currentHP = startingHP;

		//Initializes the player to the Player's GameObject
		player = GameObject.FindWithTag("Player");

		//The amount of time before the game restarts upon player death.
		deathTime = 2.0f;

		//Sets the image to use when damage is received 
		dmgImg = GameObject.FindGameObjectWithTag("RawImage").GetComponent<RawImage>();
		dmgImg.enabled = false;

		
	}

	void Update () {
		PlayerStatus();
		IsDisabled ();
	}

	public void TakeDamage(int dmg){
		//Spawners are indestructible
		if(currentHP > 0 && this.tag != "Spawner"){
			//Removes an amount of health from this object
			currentHP -= dmg;

			//If this object is the player, enable the damage image and reset its countdown time.
			if(this.tag == "Player"){
				dmgImg.enabled = true;
				dmgImgTime = .1f;
			}

			//If the player deals Damage to an Enemy that is unaware of their presence, the Enemy will move to where it was shot from.
			if(this.tag == "RangedEnemy" || this.tag == "MeleeEnemy"){
				if(this.gameObject.GetComponent<EnemyAI>().enabled == true){
					this.gameObject.GetComponent<EnemyAI>().lastKnownPos = player.transform.position;
					player.GetComponent<IntelController>().OnAlert(this.gameObject);
					this.gameObject.GetComponent<EnemyAI>().onAlert = true;
				}
			}
			else if(this.tag == "Turret"){
				if(this.gameObject.GetComponent<TurretMotor>().enabled == true){
					this.gameObject.GetComponent<TurretMotor>().lastKnownPos = player.transform.position;
					player.GetComponent<IntelController>().OnAlert (this.gameObject);
					this.gameObject.GetComponent<TurretMotor>().onAlert = true;
				}
			}

			//If this object has 0 or less than 0 HP, it will be destroyed
			if(currentHP <= 0){
				//This enables the countdown to level reset in update.
				isDead = true;

				//The distinctions made here are to add seperate death effects for the seperate enemies. 
				if(this.tag == "Turret"){
					Destroy(this.gameObject);
				}
				
				if(this.tag == "RangedEnemy" || this.tag == "MeleeEnemy"){
					Destroy(this.gameObject);
				}
				
				
			}
		}
	}

	//This function adds HP up to a maximum amount of 100
	//It is called in the pickUp Script
	public void AddHealth(int hP){
		currentHP += hP;
		if(currentHP > startingHP){
			currentHP = startingHP;
		}
	}

	//This function will disable any enemy hit by a nullifierShot.
	public void SetDisable(){
		//Amount of time the enemy is Disabled. 
		disableTime = 5.0f;

		//Disabled Enemies and Spawners appear grey.
		GetComponent<Renderer>().material.SetColor("_Color", Color.grey);

		//Disables the EnemyAI and sets stopping Distance to a large number to effectively stop an Enemy's function.
		if(this.gameObject.tag == "RangedEnemy" || this.gameObject.tag == "MeleeEnemy"){
			this.gameObject.GetComponent<EnemyAI>().enabled = false;
			this.gameObject.GetComponent<NavMeshAgent>().stoppingDistance = 10000;
		}

		//Disables the TurretMotor component if the object hit was a Turret.
		else if(this.gameObject.tag == "Turret"){
			this.gameObject.GetComponent<TurretMotor>().enabled = false;
		}

		//Disables the EnemySpawner component if the ojbect hit was a Spawner.
		else if(this.gameObject.tag == "Spawner"){
			this.gameObject.GetComponent<EnemySpawner>().enabled = false;
		}
	}

	//Returns whether or not the GameObject is dead or not.
	public bool IsDead(){
		return isDead;
	}

	//This function re-enables the appropriate enemies after a set time.
	private void IsDisabled(){
		//If this GameObject is still disabled and NOT a spawner, countdown to re-enabling. 
		//Spawners are permanently disabled.
		if(disableTime > 0 && tag != "Spawner"){
			disableTime -= Time.deltaTime;

			//When disableTime reaches 0, re-enable the GameObject's respective componenets.
			if(disableTime <= 0){
				if(this.gameObject.tag == "RangedEnemy" || this.gameObject.tag == "MeleeEnemy"){
					this.gameObject.GetComponent<EnemyAI>().enabled = true;
					if(this.gameObject.tag == "RangedEnemy"){
						this.gameObject.GetComponent<NavMeshAgent>().stoppingDistance = 6.0f;
						GetComponent<Renderer>().material.SetColor("_Color", Color.red);
					}
					if(this.gameObject.tag == "MeleeEnemy"){
						this.gameObject.GetComponent<NavMeshAgent>().stoppingDistance = 3.0f;
						GetComponent<Renderer>().material.SetColor("_Color", Color.yellow);
					}
				}
				else if(this.gameObject.tag == "Turret"){
					this.gameObject.GetComponent<TurretMotor>().enabled = true;
					GetComponent<Renderer>().material.SetColor("_Color", Color.red);
				}
			}
		}
	}

	//Keeps track of whether or not this GameObject is still alive, and if it has taken damage.
	private void PlayerStatus(){
		if (isDead) {
			//Counts down to 0 at which point the level restarts
			deathTime -= Time.deltaTime;
			if (deathTime <= 0) {
				if(this.tag == "Player"){
					Application.LoadLevel(Application.loadedLevel);
				}
			}
		}
		//Displays the dmgImg if the object taking damage is the player
		if(this.tag == "Player"){
			if(dmgImg.enabled){
				//Counts down to 0 at which point the damage img is removed
				dmgImgTime -= Time.deltaTime;
				if(dmgImgTime <= 0){
					dmgImg.enabled = false;
				}
			}
		}
	}
}
