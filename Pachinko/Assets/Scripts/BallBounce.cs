﻿using UnityEngine;
using System.Collections;

public class BallBounce : MonoBehaviour {

	public Vector3 gravity = Vector3.down * 9.81f;
	public Vector3 currentVelocity = Vector3.zero;

	private float radius;

	void Start()
	{
		//Radius of the sphere
		radius = transform.localScale.x / 2;
	}

	void Update () 
	{
		UpdatePosition();
	}

	private Vector3 CalculateNewPos(float distTrav)
	{
		Vector3 newPos;
		//Checks if there will be a collision in the next frame
		RaycastHit hitInfo; 
		if(Physics.SphereCast(transform.position, radius, currentVelocity.normalized, out hitInfo, distTrav, 1))
		{
			//Seperate the direction and speed
			Vector3 moveDir = currentVelocity.normalized;
			float speed = currentVelocity.magnitude;

			//Set position to just before the surface collision
			newPos = transform.position + moveDir * (hitInfo.distance - 0.00001f);
			transform.position = newPos;

			//Find amount distance lost
			Vector3 lostDist = (currentVelocity * Time.deltaTime) - (moveDir * hitInfo.distance);

			//Reflect the Velocity
			moveDir = Vector3.Reflect(moveDir, hitInfo.normal);
			currentVelocity = moveDir * speed;

			//Apply collision event 
			hitInfo.transform.gameObject.SendMessage("Collided", this.GetComponent<BallBounce>(), SendMessageOptions.DontRequireReceiver);

			//Check for another hit on rebound and correct for lost distance
			if(Physics.SphereCast(transform.position, radius, currentVelocity.normalized, out hitInfo, lostDist.magnitude, 1))
			{
				moveDir = currentVelocity.normalized;
				speed = currentVelocity.magnitude;
				newPos = transform.position + moveDir * (hitInfo.distance - 0.00001f);
				transform.position = newPos;
				moveDir = Vector3.Reflect(moveDir, hitInfo.normal);
				currentVelocity = moveDir * speed;
				hitInfo.transform.gameObject.SendMessage("Collided", this.GetComponent<BallBounce>(), SendMessageOptions.DontRequireReceiver);
			}
			//else move the rest of the distance
			else
			{
				newPos = transform.position + (currentVelocity.normalized * lostDist.magnitude);
			}
		}
		//If no collision detected, move normally
		else
		{
			newPos = transform.position + (currentVelocity.normalized * distTrav);
		}
		return newPos;
	}

	private void UpdatePosition()
	{

		//Sets changed velocity caused by gravity
		currentVelocity += (gravity * Time.deltaTime);
		float distTrav = currentVelocity.magnitude * Time.deltaTime;
		
		Vector3 newPos = CalculateNewPos(distTrav);
		transform.position = newPos;
	}
}
