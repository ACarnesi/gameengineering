﻿using UnityEngine;
using System.Collections;

public class ScoreZone : MonoBehaviour 
{
	public int value;
	public GameManager game;

	void OnTriggerEnter(Collider col)
	{
		//If a ball enters the trigger, add score and destroy ball.
		if(col.tag == "Ball")
		{
			game.score += value;
			Destroy(col.gameObject);
		}
	}

}
