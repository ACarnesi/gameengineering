﻿using UnityEngine;
using System.Collections;

public class GravField : MonoBehaviour 
{
		public Vector3 gravForce;
		private Vector3 gOrig;
		private BallBounce ballB;

		void OnTriggerEnter(Collider col)
		{
			//Saves original gravity of the ball that triggered, and sets gravity to predefined value.
			ballB = col.gameObject.GetComponent<BallBounce>();
			gOrig = ballB.gravity;
			if(col.tag == "Ball")
			{
				ballB.gravity = gravForce;
			}
		}

		void OnTriggerExit(Collider col)
		{
			//Resets gravity to original upon the ball leaving.
			ballB = col.gameObject.GetComponent<BallBounce>();
			ballB.gravity = gOrig;
		}
}
