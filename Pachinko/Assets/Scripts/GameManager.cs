﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour {
	
	public Text scoreText;
	public Text ballsText;
	public int score;
	public int numBalls;
	public GameObject ballPrefab;
	public LayerMask transMask; 

	void Start () {
		score = 0;
	}

	void Update () {
		//Instantiates balls upon click, while the player has balls remaining
		OnMouseClick();
		//Updates the Score and Balls remaining GUI elements.
		UpdateGUI();
	}

	private void OnMouseClick()
	{
		if(Input.GetMouseButtonDown(0))
		{
			Debug.Log ("MouseDown");
			RaycastHit hitInfo;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			
			if(Physics.SphereCast(ray, ballPrefab.transform.localScale.x/2, out hitInfo, 100, transMask))
			{
				if(hitInfo.transform.tag == "BallSpawner")
				{
					if(numBalls > 0){
						GameObject ball = Instantiate(ballPrefab, (hitInfo.point - Vector3.forward * 3), transform.rotation) as GameObject;
						numBalls--;
					}
				}
			}
		}
	}

	private void UpdateGUI()
	{
		scoreText.text = "Score: " + score.ToString();
		ballsText.text = "Balls: " + numBalls.ToString();
	}
}
