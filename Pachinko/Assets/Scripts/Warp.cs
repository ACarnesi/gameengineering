﻿using UnityEngine;
using System.Collections;

public class Warp : MonoBehaviour 
{
	public Transform warpPartner;

	void OnTriggerEnter(Collider col)
	{
		//If a ball triggers, change position to preset Transform of an object.
		if(col.tag == "Ball")
		{
			col.transform.position = warpPartner.position;
		}
	}
}
