﻿using UnityEngine;
using System.Collections;

public class Bumper : MonoBehaviour 
{
	public float boostAmt;

	void Collided(BallBounce g)
	{
		//Boosts ball velocity upon collision with bumper.
		if(g.tag == "Ball")
		{
			g.currentVelocity *= boostAmt;
		}
	}
}
