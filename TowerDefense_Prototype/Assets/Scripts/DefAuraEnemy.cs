﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DefAuraEnemy : Enemy {
	public float protDuration;
	public float protAmt;
	public float protRange;
	public int protectableLayer;

	private List<Enemy> protectionList = new List<Enemy>();

	protected override void Awake()
	{
		NotificationCenter.Default.AddObserver("OnEnemySpawn", OnEnemySpawn);
		{
			
		}
		base.Awake();

		foreach(GameObject g in GameObject.FindGameObjectsWithTag("GroundEnemy"))
		{
			if(g.layer == protectableLayer)
			{
				protectionList.Add(g.GetComponent<Enemy>());
			}
		}
		foreach(GameObject g in GameObject.FindGameObjectsWithTag("AirEnemy"))
		{
			if(g.layer == protectableLayer)
			{
				protectionList.Add(g.GetComponent<Enemy>());
			}
		}
	}

	protected override void Update()
	{
		base.Update();
		foreach(Enemy e in protectionList)
		{
			if(e != null)
			{
				if(Vector3.Distance(transform.position, e.transform.position) <= protRange)
					e.AddCondition(new ConditionProtect(protAmt, protDuration, "Protection"));
			}
		}
	}

	void OnDestroy()
	{
		NotificationCenter.Default.RemoveObserver("OnEnemySpawn", OnEnemySpawn);
	}

	private void OnEnemySpawn(object o)
	{
		Enemy e = (Enemy)o;
		if(e.tag == "GroundEnemy" || e.tag == "AirEnemy")
		{
			if(e.gameObject.layer == protectableLayer)
			{
				protectionList.Add(e);
			}
		}
	}
}
