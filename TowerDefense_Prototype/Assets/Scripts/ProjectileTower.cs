﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ProjectileTower : Tower {

	public GameObject projectilePref;
	//	public AudioClip fireSound;

	protected Enemy currentTarget;
	protected Enemy selectedTarget;
	//	protected AudioSource aS;

	protected override void Awake ()
	{
		base.Awake ();
	}

	void Update()
	{
		if(!towerPlaced)
		{
			SnapToGrid();
		}
		else
		{
			PickTarget();
			if(currentTarget != null)
			{
				transform.LookAt(currentTarget.transform.position);
				AttackTarget();
			}
		}
	}

	void OnDestroy()
	{
		NotificationCenter.Default.RemoveObserver("OnEnemyDeath", OnEnemyDeath);
	}

	public void SetSelectedTarget(Enemy tar)
	{
		selectedTarget = tar;
	}

	private void PickTarget()
	{
		if(selectedTarget != null)
		{
			if(Vector3.Distance(selectedTarget.transform.position, transform.position) <= range)
			{
				currentTarget = selectedTarget;
			}
			else if(enemyList.Count > 0)
			{
				//Picks target closest to goal
				currentTarget = enemyList[0];
				for(int i = 1; i < enemyList.Count; i++)
				{
					if(currentTarget.distanceToTravel > enemyList[i].distanceToTravel)
					{
						currentTarget = enemyList[i];
					}
				}
			}
			else
			{
				currentTarget = null;
			}
		}

		else if(enemyList.Count > 0)
		{
			//Picks target closest to goal
			currentTarget = enemyList[0];
			for(int i = 1; i < enemyList.Count; i++)
			{
				if(currentTarget.distanceToTravel > enemyList[i].distanceToTravel)
				{
					currentTarget = enemyList[i];
				}
			}
		}
		else
		{
			currentTarget = null;
		}
	}

	protected override void AttackTarget()
	{
		if(cooldownTime <= 0)
		{
			GameObject projectile = Instantiate(projectilePref, transform.position + Vector3.zero, transform.rotation) as GameObject;
			IProjectile tP = projectile.GetComponent<IProjectile>();
			tP.InitializeProjectile(currentTarget, damageAmt);
			cooldownTime = attackRate;
			//			aS.clip = fireSound;
			//			aS.Play();
		}
		else
		{
			cooldownTime -= Time.deltaTime;
		}
	}
}
