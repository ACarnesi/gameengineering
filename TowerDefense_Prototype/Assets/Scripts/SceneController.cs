﻿using UnityEngine;
using System.Collections;

public class SceneController : MonoBehaviour {

	private enum controlState
	{
		Selecting,
		PlacingTower
	};

	public LayerMask mask = -1;
	public float speed = 1f;

	private controlState controlS = controlState.Selecting;
	private GameObject placedTower;
	private GameObject selectedTower;
	private GameObject lastClickedTile;

	void Start()
	{
		NotificationCenter.Default.AddObserver("OnTowerPurchase", OnTowerPurchase);
	}

	void OnDestroy()
	{
		NotificationCenter.Default.RemoveObserver("OnTowerPurchase", OnTowerPurchase);
	}

	void Update()
	{
		NavigateMap();
		if(controlS == controlState.Selecting)
		{
			UseSelectState();
		}

		else if(controlS == controlState.PlacingTower)
		{
			UsePlaceState();
		}
	}

	//Used for selecting towers and moving around the map
	private void UseSelectState()
	{
		if(Input.GetKey(KeyCode.LeftShift))
		{
			if(Input.GetMouseButtonDown(0))
			{
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				
				RaycastHit hitInfo;
				if(Physics.Raycast(ray, out hitInfo, 100, mask))
				{
					if(hitInfo.transform.tag == "GroundEnemy" || hitInfo.transform.tag == "AirEnemy")
					{
						Enemy e = hitInfo.transform.GetComponent<Enemy>();
						foreach(GameObject g in GameObject.FindGameObjectsWithTag("Tower"))
						{
							if(g.GetComponent<ProjectileTower>() != null)
								g.GetComponent<ProjectileTower>().SetSelectedTarget(e);
						}
					}
				}
			}
		}

		//If you left click check what you clicked
		else if(Input.GetMouseButtonDown(0))
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if(!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
			{
				RaycastHit hitInfo;
				if(Physics.Raycast(ray, out hitInfo, 100, mask))
				{
					//if you hit a tower, visualize the selection, and disable the visualization on a previously selected tower
					if(hitInfo.transform.tag == "Tower")
					{
						if(selectedTower != null)
						{
							selectedTower.GetComponent<Tower>().rangeIndicator.enabled = false;
						}
						selectedTower = hitInfo.transform.gameObject;
						NotificationCenter.Default.PostNotification("OnTowerSelect", selectedTower.GetComponent<Tower>());
						selectedTower.GetComponent<Tower>().rangeIndicator.enabled = true;
					}
					//Else if you hit an enemy and have a tower selected, target that enemy with the tower
					else if(hitInfo.transform.tag == "GroundEnemy" || hitInfo.transform.tag == "AirEnemy" && selectedTower != null)
					{	
						if(selectedTower != null)
						{
							if(selectedTower.GetComponent<ProjectileTower>() != null)
								selectedTower.GetComponent<ProjectileTower>().SetSelectedTarget(hitInfo.transform.GetComponent<Enemy>());
						}
					}
					//Otherwise cancel the selection
					else
					{
						if(selectedTower != null)
						{
							selectedTower.GetComponent<Tower>().rangeIndicator.enabled = false;
							selectedTower = null;
							NotificationCenter.Default.PostNotification("OnTowerDeselect");
						}
					}
				}
			}
		}


		//Cancel any selection on right mouse button down
		else if(Input.GetMouseButtonDown(1))
		{
			if(selectedTower != null)
			{
				selectedTower.GetComponent<Tower>().rangeIndicator.enabled = false;
				selectedTower = null;
				NotificationCenter.Default.PostNotification("OnTowerDeselect");
			}
		}
	}

	//Used to place towers and is the used state when a tower is selected to be bought in the GUI
	private void UsePlaceState()
	{
		if(Input.GetMouseButtonDown(0))
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			RaycastHit hitInfo;
			if(Physics.Raycast(ray, out hitInfo, 100, mask))
			{
				if(hitInfo.transform.tag == "Placeable")
				{
					if(!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
					{
						Tower t = placedTower.GetComponent<Tower>();
						t.towerPlaced = true;
						t.gameObject.layer = 0;
						controlS = controlState.Selecting;
						t.transform.parent = hitInfo.transform;
						t.rangeIndicator.enabled = false;
						NotificationCenter.Default.PostNotification("OnTowerPlaced", placedTower);
					}
				}
			}
		}

		else if(Input.GetMouseButtonDown(1))
		{
			Destroy(placedTower);
			controlS = controlState.Selecting;
		}
	}

	private void OnTowerPurchase(object o)
	{
		//If you aren't already placing a tower
		if(controlS != controlState.PlacingTower)
		{
			controlS = controlState.PlacingTower;
			//if the object exists, instantiate it		
			if(o != null)
			{
				placedTower = Instantiate((GameObject)o) as GameObject;
			}
		}

		//Replace the current tower with the new one
		else
		{
			Destroy(placedTower);
			if(o != null)
			{
				placedTower = Instantiate((GameObject)o) as GameObject;
			}
		}
	}

	//Moves based on mouse position on screen
	private void NavigateMap()
	{	#region Keyboard Camera Controlls
		//Moves camera Right
		if(Input.GetKey(KeyCode.RightArrow))
		{
			Ray right = Camera.main.ScreenPointToRay(new Vector3(Screen.width, Screen.height / 2, 0));

			RaycastHit hitInfo;
			if(Physics.Raycast(right, out hitInfo, 100, mask))
			{
				transform.position += Vector3.right * Time.deltaTime * speed;
			}
		}

		//Moves camera Left
		if(Input.GetKey(KeyCode.LeftArrow))
		{
			Ray left = Camera.main.ScreenPointToRay(new Vector3(0, Screen.height / 2, 0));

			RaycastHit hitInfo;
			if(Physics.Raycast(left, out hitInfo, 100, mask))
			{
				transform.position += Vector3.left * Time.deltaTime * speed;
			}
		}

		//Moves camera Up
		if(Input.GetKey(KeyCode.UpArrow))
		{
			Ray up = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height, 0));

			RaycastHit hitInfo;
			if(Physics.Raycast(up, out hitInfo, 100, mask))
			{
				transform.position += Vector3.forward * Time.deltaTime * speed;
			}
		}

		//Moves camera Down
		if(Input.GetKey(KeyCode.DownArrow))
		{
			Ray down = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, 0, 0));

			RaycastHit hitInfo;
			if(Physics.Raycast(down, out hitInfo, 100, mask))
			{
				transform.position += Vector3.back * Time.deltaTime * speed;
			}
		}
		#endregion
	}
}
