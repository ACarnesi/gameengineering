﻿using UnityEngine;
using UnityEngine.UI;
//using UnityEngine.SceneManagement;
using System.Collections;

public class GUIContorl : MonoBehaviour {

	public int goldCount = 100;
	public int currentLife = 10;
	public Text goldText;
	public Text lifeText;
	public Text levelText;
	public Text gameEndText;
	public Tower selectedTower;
	public GameObject sellTowerButton;
	public GameObject gameOverPanel;

	private bool playerWon;
	private string levelName;

	void Start()
	{
		NotificationCenter.Default.AddObserver("OnTowerPlaced", OnTowerPlaced);
		NotificationCenter.Default.AddObserver("OnEnemyDeath", OnEnemyDeath);
		NotificationCenter.Default.AddObserver("OnEnemyScored", OnEnemyScored);
		NotificationCenter.Default.AddObserver("OnGameOver", OnGameOver);
		NotificationCenter.Default.AddObserver("OnTowerSelect", OnTowerSelect);
		NotificationCenter.Default.AddObserver("OnTowerDeselect", OnTowerDeselect);
		#if UNITY_EDITOR
//		levelName = "Level: " + UnityEditor.SceneManagement.EditorSceneManager.GetActiveScene().name.ToString();
		#endif
		levelName = "Level: " + UnityEditor.EditorApplication.currentScene.ToString ();
		goldText.text = ("Gold: " + goldCount.ToString());
		lifeText.text = ("Life: " + currentLife.ToString());
		gameOverPanel.SetActive(false);
		playerWon = true;
		levelText.text = levelName;
		sellTowerButton.SetActive(false);
	}

	void OnDestroy()
	{
		NotificationCenter.Default.RemoveObserver ("OnTowerPlaced", OnTowerPlaced);
		NotificationCenter.Default.RemoveObserver ("OnEnemyDeath", OnEnemyDeath);
		NotificationCenter.Default.RemoveObserver ("OnEnemyScored", OnEnemyScored);
		NotificationCenter.Default.RemoveObserver ("OnGameOver", OnGameOver);
		NotificationCenter.Default.RemoveObserver("OnTowerSelect", OnTowerSelect);
		NotificationCenter.Default.RemoveObserver("OnTowerDeselect", OnTowerDeselect);
	}

	//Selects tower to purchase&place if the gold requirement is met.
	public void BuildTurretButton(GameObject tower)
	{
		if(tower.GetComponent<Tower>() != null)
		{
			Tower towerType = tower.GetComponent<Tower>();
			if(goldCount >= towerType.goldCost)
			{
				NotificationCenter.Default.PostNotification("OnTowerPurchase", tower);
			}
		}
	}
	
	public void ButtonSellTower()
	{
		sellTowerButton.SetActive(false);
		goldCount += (int)(selectedTower.goldCost * .75f);
		Destroy(selectedTower.gameObject);
		goldText.text = ("Gold: " + goldCount.ToString());
		selectedTower = null;
	}

	public void ButtonExitApp()
	{
		#if UNITY_EDITOR
		{
			UnityEditor.EditorApplication.isPlaying = false;
		}
		#else
		Application.Quit();
		#endif
	}

	public void ButtonRestartLevel()
	{
		Application.LoadLevel (Application.loadedLevel);
		#if UNITY_EDITOR
//		UnityEditor.SceneManagement.EditorSceneManager.LoadScene(UnityEditor.SceneManagement.EditorSceneManager.GetActiveScene().name);
		#else
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		#endif
	}

	//If the tower is placed, then remove gold from total.
	private void OnTowerPlaced(object o)
	{
		GameObject t = (GameObject)o;
		Tower towerType = t.GetComponent<Tower>();
		goldCount -= towerType.goldCost;
		goldText.text = ("Gold: " + goldCount.ToString());
	}

	private void OnTowerSelect(object o)
	{
		selectedTower = (Tower)o;
		sellTowerButton.SetActive(true);
	}

	private void OnTowerDeselect(object o)
	{
		selectedTower = null;
		sellTowerButton.SetActive(false);
	}

	private void OnEnemyDeath(object o)
	{
		Enemy e = (Enemy) o;
		goldCount += e.enemyWorth;
		goldText.text = ("Gold: " + goldCount.ToString());
	}

	private void OnEnemyScored(object o)
	{
		Enemy e = (Enemy) o;
		currentLife -= e.enemyDamage;
		lifeText.text = ("Life: " + currentLife.ToString());
		if(currentLife <= 0)
		{
			playerWon = false;
			NotificationCenter.Default.PostNotification("OnGameOver", playerWon);
			Time.timeScale = 0;
		}
	}

	private void OnGameOver(object o)
	{
		bool win = (bool)o;
		if(win)
		{
			gameEndText.text = "You Win!";
		}
		else
		{
			gameEndText.text = "Game Over";
		}
		gameOverPanel.SetActive(true);
	}
}
