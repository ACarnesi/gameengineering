﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Tower : MonoBehaviour {

	public int goldCost;
	public float range = 1;
	public float damageAmt = 1;
	public float attackRate = 1;
	public bool towerPlaced;
	public LayerMask mask = -1;
	public MeshRenderer rangeIndicator;
//	public AudioClip fireSound;

	protected float cooldownTime;
	protected List<Enemy> enemyList = new List<Enemy>();
	protected SphereCollider towerCol;
//	protected AudioSource aS;

	protected virtual void Awake()
	{
		NotificationCenter.Default.AddObserver("OnEnemyDeath", OnEnemyDeath);
		rangeIndicator.transform.localScale = new Vector3(range * 2, rangeIndicator.transform.localScale.y, range * 2);
		towerPlaced = false;
		SnapToGrid();
		towerCol = GetComponent<SphereCollider>();
		towerCol.radius = range;
		cooldownTime = 0;
//		aS = new AudioSource();
	}

	void Update()
	{
		if(!towerPlaced)
		{
			SnapToGrid();
		}
	}

	void OnTriggerEnter(Collider col)
	{
		if(col.tag == "GroundEnemy" || col.tag == "AirEnemy")
		{
			enemyList.Add(col.GetComponent<Enemy>());
		}
	}

	void OnTriggerExit(Collider col)
	{
		if(col.tag == "GroundEnemy" || col.tag == "AirEnemy")
		{
			enemyList.Remove(col.GetComponent<Enemy>());
		}
	}

	void OnDestroy()
	{
		NotificationCenter.Default.RemoveObserver("OnEnemyDeath", OnEnemyDeath);
	}

	protected void SnapToGrid()
	{
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		RaycastHit hitInfo;
		if(Physics.Raycast(ray, out hitInfo, 1000, mask))
		{
			Vector3 placePos = hitInfo.point;
				
			placePos.x = Mathf.Round(placePos.x);
			placePos.y = hitInfo.transform.position.y + 1;
			placePos.z = Mathf.Round(placePos.z);

			transform.position = placePos;
		}
	}

	protected virtual void AttackTarget()
	{
		
	}

	protected void OnEnemyDeath(object o)
	{
		Enemy e = (Enemy) o;
		if(enemyList.Contains(e))
		{
			enemyList.Remove(e);
		}
	}
}
