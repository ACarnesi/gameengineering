﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SlowTower : Tower{

	public float slowPower;
	public float slowDuration;

	private ParticleSystem pS;

	protected override void Awake()
	{
		base.Awake();
		pS = this.GetComponent<ParticleSystem>();
	}

	void Update()
	{
		
		if(!towerPlaced)
			SnapToGrid();
		else
		{
			AttackTarget();
		}
	}

	void OnDestroy()
	{
		NotificationCenter.Default.RemoveObserver("OnEnemyDeath", OnEnemyDeath);
	}

	protected virtual void AttackTarget()
	{
		if(enemyList.Count > 0)
		{
			if(cooldownTime <= 0)
			{
				cooldownTime = attackRate;
				for(int i = 0; i < enemyList.Count; i++)
				{
					enemyList[i].AddCondition(new ConditionSlow(slowPower, slowDuration, "Slow"));
					enemyList[i].TakeDamage(damageAmt);
				}
			}
			else
			{
				cooldownTime -= Time.deltaTime;
			}
		}
	}
}
