﻿using UnityEngine;
using System.Collections;

public class ConditionProtect : ICondition {
	public float duration;
	public float dmgResist;

	public ConditionProtect(float portAmt, float dur, string condRef)
	{
		dmgResist = portAmt;
		duration = dur;
		conditionRef = condRef;
	}

	public override void InflictCondition(Enemy e, float deltaT)
	{
		if(duration > 0)
		{
			e.SetResistance(e.damageResistance + dmgResist);
			duration -= deltaT;
		}
		else
		{
			e.SetResistance(e.damageResistance);
			e.conditionList.Remove(this);
		}
	}
}
