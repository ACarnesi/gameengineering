﻿using UnityEngine;
using System.Collections;

public class EnemyGoal : MonoBehaviour {

	void OnTriggerEnter(Collider col)
	{
		if(col.tag == "GroundEnemy" || col.tag == "AirEnemy")
		{
			NotificationCenter.Default.PostNotification("OnEnemyScored", col.GetComponent<Enemy>());
			Destroy(col.gameObject);
		}
	}
}
