﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Enemy : MonoBehaviour {

	public float enemyHealth;
	public float startingHealth;
	public float enemySpeed;
	public float distanceToTravel;
	public float damageResistance;
	public int enemyWorth;
	public int enemyDamage = 1;
	public List<ICondition> conditionList = new List<ICondition>();

	protected float currentSpeed;
	protected float currentResistance;
	protected bool isDead;
	protected Vector3 targetPos;
	protected SpriteRenderer healthBar;
	protected Queue<Vector3> queuedPath = new Queue<Vector3>();

	protected virtual void Awake()
	{
		isDead = false;
		currentSpeed = enemySpeed;
		enemyHealth = startingHealth;
		healthBar = this.GetComponentInChildren<SpriteRenderer>();
	}

	protected virtual void Update()
	{
		if(!isDead)
		{
			MoveTo(targetPos);
			UpdateConditions();
		}
	}

	public void InitializePath(Vector3[] path)
	{
		for(int i = 0; i < path.Length; i++)
		{
			queuedPath.Enqueue(path[i]);
		}
		if(queuedPath.Count != 0)
		{
			targetPos = queuedPath.Dequeue();
		}
		FindDistanceToTravel(path);
	}

	private void MoveTo(Vector3 pos)
	{
		Vector3 moveDir = (pos - transform.position).normalized;
		Vector3 moveDist = currentSpeed * moveDir * Time.deltaTime;
		if(moveDist.magnitude >= (Vector3.Distance(transform.position, pos)))
		{
			transform.position = pos;
			if(queuedPath.Count != 0)
			{
				targetPos = queuedPath.Dequeue(); 
			}
		} 
		else 
		{
			transform.position += moveDist;
			distanceToTravel -= moveDist.magnitude;
		}
	}
		
	private void FindDistanceToTravel(Vector3[] path)
	{
		for(int i = 0; i < path.Length - 1; i++)
		{
			distanceToTravel += Vector3.Distance(path[i], path[i+1]);
		}
	}

	private void UpdateConditions()
	{
		for(int i = 0; i < conditionList.Count; i++)
		{
			conditionList[i].InflictCondition(this, Time.deltaTime);
		}
	}

	public void AddCondition(ICondition c)
	{
		for(int i = 0; i < conditionList.Count; i++)
		{
			if(conditionList[i].conditionRef == c.conditionRef)
			{
				conditionList[i] = c;
				return;
			}
		}
		conditionList.Add(c);
	}

	public virtual void TakeDamage(float dmgAmt)
	{
		if(dmgAmt - currentResistance <= 0)
		{
			return;
		}
		else
		{
			enemyHealth -= (dmgAmt - currentResistance);
			if(healthBar != null && !isDead)
				healthBar.transform.localScale = new Vector3(enemyHealth/startingHealth, healthBar.transform.localScale.y, healthBar.transform.localScale.z);
			if(enemyHealth <= 0)
			{
				if(!isDead)
				{
					Destroy(this.gameObject, .25f);
					NotificationCenter.Default.PostNotification("OnEnemyDeath", this);
					isDead = true;
				}
			}
		}
	}

	public virtual int GetNumEnemies()
	{
		return 1;
	}

	public void SetSpeed(float s)
	{
		currentSpeed = s;
	}

	public float GetSpeed()
	{
		return currentSpeed;
	}

	public void SetResistance(float r)
	{
		currentResistance = r;
	}

	public float GetResistance()
	{
		return currentResistance;
	}
}
