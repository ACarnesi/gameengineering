﻿using UnityEngine;
using System.Collections;

public abstract class IProjectile : MonoBehaviour {
	public abstract void InitializeProjectile(Enemy target, float damage);
}
