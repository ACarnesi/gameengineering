﻿using UnityEngine;
using System.Collections;

public abstract class ICondition{
	public float duration;
	public string conditionRef;
	public abstract void InflictCondition(Enemy e, float deltaT);
}
