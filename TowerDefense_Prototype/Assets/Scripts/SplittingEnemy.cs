﻿using UnityEngine;
using System.Collections;

public class SplittingEnemy : Enemy {

	public GameObject splitEnemies;
	public int numOfSplits;

	private Vector3 GetRandomSpawnPosition()
	{
		float randomX = Random.Range(transform.position.x - 1f, transform.position.x + 1f);
		float randomY = Random.Range(transform.position.y - 1f, transform.position.y + 1f);
		float randomZ = Random.Range(transform.position.z - 1f, transform.position.z + 1f);
		return new Vector3(randomX, randomY, randomZ);
	}

	public override void TakeDamage(float dmgAmt)
	{
		if(dmgAmt - currentResistance <= 0)
		{
			return;
		}
		else
		{
			enemyHealth -= (dmgAmt - currentResistance);
			if(healthBar != null && !isDead)
				healthBar.transform.localScale = new Vector3(enemyHealth/startingHealth, healthBar.transform.localScale.y, healthBar.transform.localScale.z);
			if(enemyHealth <= 0)
			{
				if(!isDead)
				{
					Vector3[] path = GetNewPath();
					for(int i = 0; i < numOfSplits; i++)
					{
						GameObject splitEnemy = Instantiate(splitEnemies, GetRandomSpawnPosition(), transform.rotation) as GameObject;
						splitEnemy.GetComponent<Enemy>().InitializePath(path);
					}
					Destroy(this.gameObject, .1f);
					NotificationCenter.Default.PostNotification("OnEnemyDeath", this);
					isDead = true;
				}
			}
		}
	}

	public override int GetNumEnemies()
	{
		return 1 + numOfSplits;
	}

	private Vector3[] GetNewPath()
	{
		Vector3[] path = new Vector3[queuedPath.Count+1];
		path[0] = targetPos;
		for(int i = 1; i < path.Length; i++)
		{
			path[i] = queuedPath.Dequeue();
		}
		return path;
	}
}
