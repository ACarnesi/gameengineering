﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Wave : System.Object  {
	public List<GameObject> enemyList = new List<GameObject>();
	public List<int> enemyCount = new List<int>();
}
