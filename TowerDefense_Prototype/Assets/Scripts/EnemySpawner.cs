﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawner : MonoBehaviour {
	public float nextWaveTimer;
	public float spawnBuffer;
	public Vector3[] groundEnemyPath;
	public Vector3[] airEnemyPath;
	public List<Wave> waveList = new List<Wave>();

	private int currentWave = 0;
	public int enemiesRemaining;
	private float waveTimerBuffer;
	private float spawnTimeBuffer;
	private bool waveStarted;

	void Start()
	{
		NotificationCenter.Default.AddObserver("OnEnemyDeath", OnEnemyDeath);
		NotificationCenter.Default.AddObserver("OnEnemyScored", OnEnemyScored);
		waveStarted = false;
		currentWave = 0;
		waveTimerBuffer = nextWaveTimer;
		spawnTimeBuffer = 0;
	}

	void Update()
	{
		if(!waveStarted)
		{
			//If time is left before next wave update time remaining, else start the wave and spawn it.
			if(waveTimerBuffer > 0)
			{
				waveTimerBuffer -= Time.deltaTime;
			}
			else
			{
				waveTimerBuffer = nextWaveTimer;
				waveStarted = true;
				if(waveList.Count > currentWave)
				{
					StartCoroutine(SpawnWave(currentWave));
				}
			}
		}
	}

	void OnDrawGizmos()
	{
		for(int i = 1; i < groundEnemyPath.Length; i++)
		{
			Gizmos.color = Color.red;
			Gizmos.DrawLine(groundEnemyPath[i - 1], groundEnemyPath[i]);
		}
		for(int i = 1; i < airEnemyPath.Length; i++)
		{
			Gizmos.color = Color.blue;
			Gizmos.DrawLine(airEnemyPath[i - 1], airEnemyPath[i]);
		}
	}

	void OnDestroy()
	{
		NotificationCenter.Default.RemoveObserver("OnEnemyDeath", OnEnemyDeath);
		NotificationCenter.Default.RemoveObserver("OnEnemyScored", OnEnemyScored);
	}

	private IEnumerator SpawnWave(int waveNum)
	{
		if(waveList[waveNum].enemyCount.Count > 0 && waveList[waveNum].enemyList.Count > 0)
		{
			int numEnemyTypes = waveList[waveNum].enemyList.Count;
			for(int i = 0; i < numEnemyTypes; i++)
			{
				int numCurrentEnemy = waveList[waveNum].enemyCount[i];
				while(numCurrentEnemy > 0)
				{
					if(spawnTimeBuffer <= 0)
					{
						GameObject newEnemy = Instantiate(waveList[waveNum].enemyList[i], transform.position, transform.rotation) as GameObject;
						NotificationCenter.Default.PostNotification("OnEnemySpawn", newEnemy.GetComponent<Enemy>());
						enemiesRemaining += newEnemy.GetComponent<Enemy>().GetNumEnemies();
						if(newEnemy.tag == "GroundEnemy")
						{
							newEnemy.GetComponent<Enemy>().InitializePath(groundEnemyPath);
						}
						else if(newEnemy.tag == "AirEnemy")
						{
							newEnemy.GetComponent<Enemy>().InitializePath(airEnemyPath);
						}
						numCurrentEnemy--;
						spawnTimeBuffer = spawnBuffer;
					}
					else
					{
						spawnTimeBuffer -= Time.deltaTime;
					}
					yield return null;
				}
			}
		}
		while(enemiesRemaining > 0)
		{
			yield return null;
		}
		waveStarted = false;
		currentWave++;
		if(currentWave >= waveList.Count)
		{
			NotificationCenter.Default.PostNotification("OnGameOver", true);
		}
	}

	private void OnEnemyDeath(object o)
	{
		enemiesRemaining--;
	}

	private void OnEnemyScored(object o)
	{
		Enemy e = (Enemy)o;
		enemiesRemaining -= e.GetNumEnemies();
	}
}
