﻿using UnityEngine;
using System.Collections;

public class TowerProjectile : IProjectile {

	public float startSpeed;

	private Enemy targetEnemy;
	private Vector3 targetPos;
	private Vector3 currentVel;
	private float currentSpeed;
	private float dmgAmt;

	void Awake()
	{
		currentSpeed = startSpeed;
	}

	// Update is called once per frame
	void Update () {
		if(targetEnemy != null)
		{
			targetPos = targetEnemy.transform.position;
			MoveToTarget(targetPos);
		}
		else
		{
			MoveToTarget(targetPos);
		}
	}

	public override void InitializeProjectile(Enemy target, float damage)
	{
		targetEnemy = target;
		dmgAmt = damage;
	}

	private void MoveToTarget(Vector3 t)
	{
		Vector3 moveDir = (t - transform.position).normalized;
		currentVel = moveDir * currentSpeed * Time.deltaTime;
		currentSpeed *= 1.05f;

		if(targetEnemy != null)
		{
			if(currentVel.magnitude >= Vector3.Distance(transform.position, t))
			{
				transform.position = t;
				targetEnemy.TakeDamage(dmgAmt);
				Destroy(this.gameObject);
			}
			
			else
			{
				transform.position += currentVel;
			}
		}
		else
		{
			if(currentVel.magnitude >= Vector3.Distance(transform.position, t))
			{
				transform.position = t;
				Destroy(this.gameObject);
			}

			else
			{
				transform.position += currentVel;
			}
		}
	}
}
