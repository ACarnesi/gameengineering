﻿using UnityEngine;
using System.Collections;

public class ConditionSlow : ICondition {
	public float duration;
	public float slowPower;

	public ConditionSlow(float slowAmt, float dur, string condRef)
	{
		slowPower = slowAmt;
		duration = dur;
		conditionRef = condRef;
	}

	public override void InflictCondition(Enemy e, float deltaT)
	{
		if(duration > 0)
		{
			e.SetSpeed(e.enemySpeed * slowPower);
			duration -= deltaT;
		}
		else
		{
			e.SetSpeed(e.enemySpeed);
			e.conditionList.Remove(this);
		}
	}
}
