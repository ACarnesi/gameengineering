﻿using UnityEngine;
using System.Collections;

public class LaserShot : IProjectile {

	public float laserSpeed;
	public float laserDistance;

	private float dmgAmt;

	// Update is called once per frame
	void Update () {
		FireLaser();
	}

	void OnTriggerEnter(Collider col)
	{
		if(col.tag == "GroundEnemy" || col.tag == "AirEnemy")
		{
			col.GetComponent<Enemy>().TakeDamage(dmgAmt);
		}
	}

	public override void InitializeProjectile(Enemy target, float damage)
	{
		dmgAmt = damage;
	}

	private void FireLaser()
	{
		if(transform.localScale.z < laserDistance)
		{
			transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z + (Time.deltaTime * laserSpeed));
		}
		else
		{
			Destroy(this.gameObject);
		}
	}
}
