﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TowerButtonGUI : MonoBehaviour {

	public Text goldCostText;
	public Text rangeText;
	public Text damageText;

	public Tower towerInfo;

	void Awake()
	{
		goldCostText.text = "Cost: " + towerInfo.goldCost.ToString();
		rangeText.text = "Range: " + towerInfo.range.ToString();
		damageText.text = "Dmg " + towerInfo.damageAmt.ToString();
	}
}
